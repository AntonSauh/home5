import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *Viited
 *Osaliselt kasutatud: http://enos.itcollege.ee/~ylari/I231/Node.java
 **/

public class Node {

    private String name;
    private Node firstChild;
    private Node nextSibling;

    public Node(String name, Node firstChild, Node nextSibling) {
        this.name = name;
        this.firstChild = firstChild;
        this.nextSibling = nextSibling;
    }

    Node(String n) {
        name = n;
    }

    public static Node parsePostfix(String s) {
        char[] characterArrayFromInput = s.toCharArray();
        Map<Integer, List<Node>> currentChildrensMap = new HashMap<>();
        int depth = 0;
        List<Node> currentSiblings = new ArrayList<>();
        StringBuilder currentName = new StringBuilder();
        List<Node> needsParentAssignment = new ArrayList<>();
        boolean needsParentAssigned = false;
        int index = 0;
        Node rootNode = null;

        validateInput(characterArrayFromInput, s);

        for (char character : characterArrayFromInput) {
            if (character == '(') {
                if(!currentName.toString().equals("")){
                    throw new RuntimeException("Comma missing between siblings! " + s);
                }
                depth++;
                if (!currentChildrensMap.containsKey(depth)) {
                    currentChildrensMap.put(depth, new ArrayList<>());
                }
                currentSiblings = currentChildrensMap.get(depth);
                currentName = new StringBuilder();
            } else if (character == ',') {
                validateNodeName(currentName.toString(), s);
                Node newNode = addNewNodeAndSetSibling(currentName.toString(), currentSiblings);
                currentSiblings.add(newNode);
                if (needsParentAssigned) {
                    newNode.firstChild = needsParentAssignment.get(needsParentAssignment.size() - 1);
                    needsParentAssigned = false;
                }
                currentName = new StringBuilder();
            } else if (character == ')') {
                validateNodeName(currentName.toString(), s);
                Node newNode = addNewNodeAndSetSibling(currentName.toString(), currentSiblings);
                if (needsParentAssigned) {
                    newNode.firstChild = needsParentAssignment.get(needsParentAssignment.size() - 1);
                }
                currentSiblings.add(newNode);
                needsParentAssigned = true;
                needsParentAssignment.add(currentSiblings.get(0));
                currentName = new StringBuilder();
                currentChildrensMap.get(depth).clear();
                depth--;
                if (!currentChildrensMap.containsKey(depth)) {
                    currentChildrensMap.put(depth, new ArrayList<>());
                }
                currentSiblings = currentChildrensMap.get(depth);
            } else {
                currentName.append(character);
                if (index == characterArrayFromInput.length - 1) {
                    validateNodeName(currentName.toString(), s);
                    rootNode = new Node(currentName.toString());
                    if(currentSiblings.size() != 0){
                        throw new RuntimeException("Root node cannot have siblings!" + s);
                    }
                    if (needsParentAssigned) {
                        rootNode.firstChild = needsParentAssignment.get(needsParentAssignment.size() - 1);
                    }
                }
            }
            index++;
        }
        return rootNode;
    }

    private static Node addNewNodeAndSetSibling(String currentName, List<Node> currentChildren) {
        Node newNode = new Node(currentName);
        if (currentChildren.size() != 0) {
            currentChildren.get(currentChildren.size() - 1).nextSibling = newNode;
        }
        return newNode;
    }

    private static void validateNodeName(String currentName, String s) {
        if(currentName.equals("")){
            throw new RuntimeException("Node name cannot be empty" + s);
        }
        if(currentName.contains(" ")){
            throw new RuntimeException("Node name cannot containt empty spaces " + s);
        }
    }

    private static void validateInput(char[] characterArrayFromInput, String inputString) {
        if(inputString.contains("\t")){
            throw new RuntimeException("Tabulation is not allowed in input string! " + inputString);
        }
        int openingBracketCount = 0;
        int closingBracketCount = 0;
        for(char c : characterArrayFromInput){
            if(c == '('){
                openingBracketCount++;
            }
            if(c == ')'){
                closingBracketCount++;
            }
        }

        if(openingBracketCount != closingBracketCount){
            throw new RuntimeException("Input Format is incorrect" + inputString);
        }

    }


    public String leftParentheticRepresentation() {

        StringBuilder ret = new StringBuilder();
        Node node = firstChild;
        ret.append(name);
        if (firstChild != null)
            ret.append('(');
        while (node != null) {
            ret.append(node.leftParentheticRepresentation());
            node = node.nextSibling;
            if (node != null)
                ret.append(',');
        }
        if (firstChild != null)
            ret.append(')');
        return ret.toString();
    }

    public static void main(String[] param) {

        String s = "((A)B,(C)D)E";
        Node t = Node.parsePostfix(s);
        String v = t.leftParentheticRepresentation();
        System.out.println(s + " ==> " + v); // (B1,C)A ==> A(B1,C)

    }
}
